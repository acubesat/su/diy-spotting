;Note: Z axis coordinates are changed among water and cells to be able to change with app easily
G21 ; units in mm
G90 ; switch off relative mode
G83
G17 
; 3rd collumn
    ; F3 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; F3 spotting x5
            G01 X153 Y153 Z44 F600

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 






    ; E3 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; E3 spotting x5
            G01 X 153 Y162 Z44 F600 ; go to the E3 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 




    ; D3 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; D3 spotting x5
            G01 X 153 Y171 Z44 F600 ; go to the F4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 





    ; C3 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; C3 spotting x5
            G01 X 153 Y180 Z44 F600 ; go to the E5 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 




    ; B3 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; B3 spotting 
            G01 X153 Y189 Z44 F600 ; go to the F4 well


            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 



    ; A3 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; A3 spotting x5
            G01 X 153 Y198 Z44 F600 ; go to the E5 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 






; 4th collumn
    ; F4 process
         ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; F4 spotting x10
            G01 X 162 Y153 Z44 F600 ; go to the F4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 




    ; E4 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; E4 spotting x10
            G01 X 162 Y162 Z44 F600 ; go to the E4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 




    ; D4 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; D4 spotting x25
            G01 X 162 Y171 Z44 F600 ; go to the F4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 




    ; C4 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; C4 spotting x5
            G01 X 162 Y180 Z44 F600 ; go to the E5 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 



    ; B4 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; B4 spotting x25
            G01 X 162 Y189 Z44 F600 ; go to the F4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 




    ; A4 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; A4 spotting x5
            G01 X 162 Y198 Z44 F600 ; go to the E5 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 



; 5th collumn
    ; F5 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; F5 spotting x25
            G01 X 171 Y153 Z44 F600 ; go to the F4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 



    ; E5 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; E5 spotting x25
            G01 X 171 Y162 Z44 F600 ; go to the E5 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 


    ; D5 process
         ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; D5 spotting x25
            G01 X 171 Y171 Z44 F600 ; go to the F4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

    ; C5 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; C5 spotting x5
            G01 X 171 Y180 Z44 F600 ; go to the E5 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

    ; B5 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; B5 spotting x25
            G01 X 171 Y189 Z44 F600 ; go to the F4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 


    ; A5 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; A5 spotting x25
            G01 X 171 Y198 Z44 F600 ; go to the E5 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 


; 6th collumn
    ; F6 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; F6 spotting 
            G01 X 180 Y153 Z44 F600 ; go to the F6 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 




    ; E6 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; E6 spotting x50
            G01 X 180 Y162 Z44 F600 ; go to the F6 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 



    ; D6 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; D6 spotting x25
            G01 X 180 Y171 Z44 F600 ; go to the F4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 





    ; C6 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; C6 spotting x5
            G01 X 180 Y180 Z44 F600 ; go to the E5 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 



    ; B6 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; B6 spotting x25
            G01 X 180 Y189 Z44 F600 ; go to the F4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 



    ; A6 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; A6 spotting x5
            G01 X 180 Y198 Z44 F600 ; go to the E5 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 



; 7th collumn
    ; F7 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; F7 spotting x80
            G01 X 189 Y153 Z44 F600 ; go to the F7 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 




    ; E7 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; E7 spotting x80
            G01 X 189 Y162 Z44 F600 ; go to the E7 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 



    ; D7 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; D7 spotting x25
            G01 X 189 Y171 Z44 F600 ; go to the F4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 



    ; C7 process
         ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; C7 spotting x5
            G01 X 189 Y180 Z44 F600 ; go to the E5 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 



    ; B7 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; B7 spotting x25
            G01 X 189 Y189 Z44 F600 ; go to the F4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 



    ; A7 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; A7 spotting x5
            G01 X189 Y198 Z44 F600 ; go to the E5 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 



; 8th collumn
    ; F8 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; F8 spotting x25
            G01 X 198 Y153 Z44 F600 ; go to the F4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 


    ; E8 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; E8 spotting x25
            G01 X 198 Y162 Z44 F600 ; go to the E5 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 


    ; D8 process
         ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; D8 spotting x25
            G01 X 198 Y171 Z44 F600 ; go to the F4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

    ; C8 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; C8 spotting x5
            G01 X 198 Y180 Z44 F600 ; go to the E5 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

    ; B8 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; B8 spotting x25
            G01 X 171 Y189 Z44 F600 ; go to the F4 well

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 


    ; A8 process
        ; water clean H2
            G01 Z44 F400
            G01 X144 Y135 Z44 F600 ; go to  water
            G01 Z26 F600
            G04 S10 ; wait 10 seconds
            G01 Z44 F400

        ; fan clean        ; fan clean
            G01 X40 Y142 Z44 F600 
            G01 Z24 F600
            M106 ; turn fan on
            G04 S4 ; wait 4 seconds
            M107 ; turn fan off
            G04 S2 ; wait 2 seconds

        ; spore uptake H1
            G01 Z44 F400
            G01 X135 Y135 Z44 F600 ; go to the cells H1
            G01 Z24 F600
            G04 S2
            G01 Z44 F400

        ; A8 spotting x25
            G01 X 198 Y198 Z44 F600 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 

            G01 Z21,5 F400 
            G04 S2 
            G01 Z25 F400 




M84 ; turn off motors
G04 S12 ; wait
; Raise nozzle and present bed