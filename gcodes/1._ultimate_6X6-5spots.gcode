;Note: Z axis coordinates are changed among water and cells to be able to change with app easily
G21 ; units in mm
G90 ; switch off relative mode
G83
G17 

;3rd collumn
    ; F3 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40

        ;F3 spotting x5
            G01 X153 Y150.5 Z44 F600 ; go to the F4 well
            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; E3 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; E3 spotting x5
            G01 X153 Y159.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29

    ; D3 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; D3 spotting x5
            G01 X153 Y168.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29

    ; C3 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; C3 spotting x5
            G01 X153 Y177.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; B3 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; B3 spotting x5
            G01 X153 Y186.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29

    ; A3 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; A3 spotting x5
            G01 X153 Y195.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


;4th
    ; F4 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; F4 spotting x5
            G01 X162 Y150.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29

    ; E4 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; E4 spotting x5
            G01 X162 Y159.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; D4 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40

        ; D4 spotting x5
            G01 X162 Y168.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; C4 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; C4 spotting x5
            G01 X162 Y177.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; B4 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40

        ; B4 spotting x5
            G01 X162 Y186.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; A4 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; A4 spotting x5
            G01 X162 Y195.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29

;5th
    ; F5 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40

        ; F5 spotting x5
            G01 X171 Y150.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; E5 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40

        ; E5 spotting x5
            G01 X171 Y159.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29

    ; D5 process
         ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; D5 spotting x5
            G01 X171 Y168.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; C5 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; C5 spotting x5
            G01 X171 Y177.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; B5 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40

        ; B5 spotting x5
            G01 X171 Y186.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; A5 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; A5 spotting x5
            G01 X171 Y195.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29

;6th
    ; F6 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40

        ; F6 spotting x5
            G01 X180 Y150.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; E6 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; E6 spotting x5
            G01 X180 Y159.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29

    ; D6 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; D6 spotting x5
            G01 X180 Y168.5 Z44 F600 ; go to the F4 well

             G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; C6 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; C6 spotting x5
            G01 X180 Y177.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29

    ; B6 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; B6 spotting x5
            G01 X180 Y186.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; A6 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; A6 spotting x5
            G01 X180 Y195.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29

;7th
    ; F7 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; F7 spotting x5
            G01 X188 Y150.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; E7 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; E7 spotting x5
            G01 X198 Y159.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29

    ; D7 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; D7 spotting x5
            G01 X207 Y168.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; C7 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; C7 spotting x5
            G01 X216 Y177.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; B7 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01	                                Z       40
        ; B7 spotting x5
            G01 X225 Y186.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; A7 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; A7 spotting x5
            G01 X233 Y195.5 Z44 F600 ; go to the E5 well

             G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


;8th
    ; F8 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; F8 spotting x5
            G01 X198 Y150.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; E8 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; E8 spotting x5
            G01 X198 Y159.5 Z44 F600 ; go to the E5 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; D8 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; D8 spotting x5
            G01 X198 Y168.5 Z44 F600 ; go to the F4 well

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; C8 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; C8 spotting x5
            G01 X198 Y177.5 Z44 F600 ; go to the E5 well

             G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; B8 process
         ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; B8 spotting x5
            G01 X198 Y186.5 Z44 F600 

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29


    ; A8 process
        ;water-cells1										
            ;wash in water										
                    G01					Z	40				
                    G01 	X	144	Y	133						
                    G01 					Z	30				
                    G04									S	2
                    G01 					Z	40				
            ;fan clean 										
                    G01					Z	40				
                    G01	X	40	Y	142						
                    G01					Z	28				
                    M106										
                    G04									S	4
                    M107										
                    G01 					Z	40				
            ;spore uptake										
                   G01					Z	40				
                   G01	X	135	Y	133						
                   G01					Z	30				
                   G04									S	2
                   G01					Z	40
        ; A8 spotting x5
            G01 X198 Y195.5 Z44 F600 

            G01 Z25
            G04 S 2
            G01 Z29

            G01 Z25
            G04 S 2
            G01 Z29
            
            G01 Z25
            G04 S 2
            G01 Z29   

            G01 Z25
            G04 S 2
            G01 Z29
                                 
            G01 Z25
            G04 S 2
            G01 Z29

G01 Z44
G01 X0 Y212
M84 ; turn off motors
; Raise nozzle and present bed


