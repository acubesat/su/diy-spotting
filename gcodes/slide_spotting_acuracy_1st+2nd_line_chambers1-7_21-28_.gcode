G21  ; units in mm 
G90 ; switch off relative mode
G17 
G01 Z40
;wash in water
    G01 X144 Y 134
    G01 Z28
                G04 S2
    G01 Z40
; fan clean
    G01 X40 Y142  F600  
    G01 Z28 F600
    M106 ; turn fan on
    G04 S4 ; wait 4 seconds
    M107 ; turn fan off
                G04 S2 ; wait 2 seconds
    G01 Z40
            G04 S2
    G01 Z40
;take cells H1
    G01 X135 Y134
    G01 Z30 
            G04 S2
                G04 S2
    G01 Z40
;spotting commence
    ;line 1(other than testline)
        ;chamber 1 (bottom left refference)
            G01 X134.4 Y 69.58 
            G01 Z35
            G01 Z30 
            G04 S2
            G04 S1
            G01 Z35
            G04 S1
        ;chamber 2
            G01 X134.823 Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;chamber3
            G01 X135.246 Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;chamber4 
            G01 X135.669 Y 69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;chamber5
            G01 X136.092 Y 69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;chamber 6
            G01 X136.515 Y 69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;chamber 7
            G01 X136.938 Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        


        ;chamber 21
            G01 X142.86Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;22
            G01 X143.283Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;23
            G01 X143.706Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;24
            G01 X144.129Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;25
            G01 X144.552Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;26
            G01 X144.975Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;27
            G01 X145.398Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;28
            G01 X145.821Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        


        ;41
            G01 X151.32Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        
        ;42
            G01 X151.743Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;43
            G01 X152.166Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;44
            G01 X152.589Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;45
            G01 X153.012Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;46
            G01 X153.435Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;47
            G01 X153.858Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;48
            G01 X154.281Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1

    ;line2

    ;wash in water
        G01 Z44
        G01 X144 Y 134
        G01 Z28
                    G04 S2
        G01 Z40
    ; fan clean
        G01 X40 Y142  F600 
        G01 Z28 F600
        M106 ; turn fan on
        G04 S4 ; wait 4 seconds
        M107 ; turn fan off
                    G04 S2 ; wait 2 seconds
        G01 Z40
    ;take cells H1
        G01 X135 Y134
        G01 Z30 
            G04 S2
                    G04 S2
        G01 Z40
    

        ;chamber 1 (bottom left refference)
            G01 X134.4 Y70.33 
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;chamber 2
            G01 X134.823 Y70.33
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;chamber3
            G01 X135.246 Y70.33
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;chamber4 
            G01 X135.669 Y70.33
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;chamber5
            G01 X136.092 Y70.33
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;chamber 6
            G01 X136.515 Y70.33
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;chamber 7
            G01 X136.938 Y70.33
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        


        ;chamber 21
            G01 X142.86 Y70.33
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;22
            G01 X143.283 Y70.33
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;23
            G01 X143.706 Y70.33
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;24
            G01 X144.129Y70.33
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;25
            G01 X144.552Y70.33
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;26
            G01 X144.975Y70.33
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;27
            G01 X145.398Y70.33
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;28
            G01 X145.821Y70.33
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        


        ;41
            G01 X151.32Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        
        ;42
            G01 X151.743Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;43
            G01 X152.166Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;44
            G01 X152.589Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;45
            G01 X153.012Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;46
            G01 X153.435Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;47
            G01 X153.858Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1
        ;48
            G01 X154.281Y69.58
            G01 Z30 
            G04 S2
            G01 Z35
            G04 S1



G01 Z45
G01 X10 Y10

M84 ; turn off motors