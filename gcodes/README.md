G21 - Sets units to millimeters
Units are in millimeters.
Prusa doesn't support inches.

G90 - Switch off relative mode
All coordinates from now on are absolute relative to the origin of the machine.
E axis is left intact.

G1 - Coordinated movement X Y Z E

Usage
G01 [ X | Y | Z | E | F | S ]

Parameters
X - The position to move to on the X-axis
Y - The position to move to on the Y-axis
Z - The position to move to on the Z-axis
E - The amount to extrude between the starting point and ending point
F - The feedrate per minute of the move between the starting point and ending point (if supplied)

G04 - Dwell:
Pause the machine for a period of time

Usage
G04 [ P | S ]

Parameters
P - Time to wait, in milliseconds
S - Time to wait, in seconds

M106 - Set fan speed

Usage
M106 [ S ]

Parameters
S - Specifies the duty cycle of the print fan. Allowed values are 0-255. If it's omitted, a value of 255 is used.

M107 - Fan off

M84 - Disable steppers

This command can be used to set the stepper inactivity timeout (S),
or to disable steppers (X, Y, Z, E).
This command can be used without any additional parameters.
In that case all steppers are disabled.

The file completeness check uses this parameter to detect an incomplete file.
It has to be present at the end of a file with no parameters.

Parameters
S - Seconds
X - X axis
Y - Y axis
Z - Z axis
E - Extruder
