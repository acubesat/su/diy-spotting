G21																																																	
G90																																																	
G17																																																	
G01 					Z	45																																											
;spots 1st line																																																	
        ;water-cells1																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;1 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.71991	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.03982	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.35973	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	135.67964	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	135.99955	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.31946	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	136.63937	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	136.95928	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	137.27919	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	137.5991	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	137.91901	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	138.23892	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	138.55883	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	138.87874	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	139.19865	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	139.51856	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	139.83847	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	140.15838	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	140.47829	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	140.7982	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	141.11811	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	141.43802	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	141.75793	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	142.07784	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	142.39775	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	142.71766	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	143.03757	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	143.35748	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	143.67739	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	143.9973	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	144.31721	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	144.63712	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	144.95703	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	145.27694	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	145.59685	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	145.91676	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	146.23667	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	146.55658	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	146.87649	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	147.1964	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	147.51631	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	147.83622	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	148.15613	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	148.47604	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	148.79595	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	149.11586	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	149.43577	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	45																																											
																																																	
;spots 2nd line																																																	
        ;water-cells2																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;2 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.71991	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.03982	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.35973	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	135.67964	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	135.99955	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.31946	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	136.63937	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	136.95928	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	137.27919	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	137.5991	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	137.91901	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	138.23892	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	138.55883	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	138.87874	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	139.19865	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	139.51856	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	139.83847	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	140.15838	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	140.47829	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	140.7982	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	141.11811	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	141.43802	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	141.75793	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	142.07784	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	142.39775	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	142.71766	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	143.03757	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	143.35748	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	143.67739	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	143.9973	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	144.31721	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	144.63712	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	144.95703	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	145.27694	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	145.59685	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	145.91676	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	146.23667	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	146.55658	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	146.87649	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	147.1964	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	147.51631	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	147.83622	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	148.15613	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	148.47604	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	148.79595	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	149.11586	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	149.43577	Y	70.467538																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	45																																											
																																																	
;spots 3rd line																																																	
        ;water-cells3																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;3 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.71991	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.03982	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.35973	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	135.67964	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	135.99955	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.31946	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	136.63937	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	136.95928	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	137.27919	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	137.5991	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	137.91901	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	138.23892	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	138.55883	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	138.87874	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	139.19865	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	139.51856	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	139.83847	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	140.15838	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	140.47829	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	140.7982	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	141.11811	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	141.43802	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	141.75793	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	142.07784	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	142.39775	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	142.71766	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	143.03757	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	143.35748	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	143.67739	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	143.9973	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	144.31721	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	144.63712	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	144.95703	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	145.27694	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	145.59685	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	145.91676	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	146.23667	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	146.55658	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	146.87649	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	147.1964	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	147.51631	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	147.83622	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	148.15613	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	148.47604	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	148.79595	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	149.11586	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	149.43577	Y	71.355076																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	45																																											
																																																	
;spots 4th line																																																	
        ;water-cells4																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;4 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.71991	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.03982	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.35973	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	135.67964	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	135.99955	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.31946	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	136.63937	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	136.95928	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	137.27919	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	137.5991	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	137.91901	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	138.23892	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	138.55883	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	138.87874	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	139.19865	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	139.51856	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	139.83847	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	140.15838	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	140.47829	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	140.7982	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	141.11811	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	141.43802	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	141.75793	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	142.07784	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	142.39775	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	142.71766	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	143.03757	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	143.35748	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	143.67739	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	143.9973	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	144.31721	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	144.63712	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	144.95703	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	145.27694	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	145.59685	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	145.91676	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	146.23667	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	146.55658	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	146.87649	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	147.1964	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	147.51631	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	147.83622	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	148.15613	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	148.47604	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	148.79595	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	149.11586	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	149.43577	Y	72.242614																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	45																																											
																																																	
;spots 5th line																																																	
        ;water-cells5																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;5 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.71991	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.03982	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.35973	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	135.67964	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	135.99955	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.31946	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	136.63937	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	136.95928	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	137.27919	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	137.5991	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	137.91901	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	138.23892	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	138.55883	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	138.87874	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	139.19865	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	139.51856	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	139.83847	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	140.15838	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	140.47829	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	140.7982	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	141.11811	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	141.43802	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	141.75793	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	142.07784	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	142.39775	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	142.71766	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	143.03757	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	143.35748	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	143.67739	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	143.9973	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	144.31721	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	144.63712	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	144.95703	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	145.27694	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	145.59685	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	145.91676	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	146.23667	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	146.55658	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	146.87649	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	147.1964	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	147.51631	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	147.83622	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	148.15613	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	148.47604	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	148.79595	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	149.11586	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	149.43577	Y	73.130152																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	45																																											
																																																	
;spots 6th line																																																	
        ;water-cells6																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;6 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.71991	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.03982	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.35973	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	135.67964	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	135.99955	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.31946	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	136.63937	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	136.95928	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	137.27919	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	137.5991	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	137.91901	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	138.23892	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	138.55883	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	138.87874	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	139.19865	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	139.51856	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	139.83847	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	140.15838	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	140.47829	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	140.7982	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	141.11811	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	141.43802	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	141.75793	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	142.07784	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	142.39775	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	142.71766	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	143.03757	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	143.35748	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	143.67739	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	143.9973	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	144.31721	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	144.63712	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	144.95703	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	145.27694	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	145.59685	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	145.91676	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	146.23667	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	146.55658	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	146.87649	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	147.1964	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	147.51631	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	147.83622	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	148.15613	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	148.47604	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	148.79595	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	149.11586	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	149.43577	Y	74.01769																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	45																																											
																																																	
;spots 7th line																																																	
        ;water-cells7																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;7 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.71991	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.03982	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.35973	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	135.67964	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	135.99955	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.31946	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	136.63937	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	136.95928	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	137.27919	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	137.5991	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	137.91901	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	138.23892	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	138.55883	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	138.87874	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	139.19865	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	139.51856	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	139.83847	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	140.15838	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	140.47829	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	140.7982	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	141.11811	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	141.43802	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	141.75793	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	142.07784	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	142.39775	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	142.71766	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	143.03757	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	143.35748	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	143.67739	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	143.9973	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	144.31721	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	144.63712	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	144.95703	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	145.27694	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	145.59685	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	145.91676	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	146.23667	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	146.55658	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	146.87649	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	147.1964	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	147.51631	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	147.83622	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	148.15613	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	148.47604	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	148.79595	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	149.11586	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	149.43577	Y	74.905228																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	45																																											
																																																	
;spots 8th line																																																	
        ;water-cells8																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;8 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.71991	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.03982	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.35973	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	135.67964	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	135.99955	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.31946	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	136.63937	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	136.95928	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	137.27919	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	137.5991	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	137.91901	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	138.23892	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	138.55883	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	138.87874	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	139.19865	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	139.51856	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	139.83847	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	140.15838	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	140.47829	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	140.7982	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	141.11811	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	141.43802	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	141.75793	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	142.07784	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	142.39775	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	142.71766	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	143.03757	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	143.35748	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	143.67739	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	143.9973	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	144.31721	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	144.63712	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	144.95703	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	145.27694	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	145.59685	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	145.91676	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	146.23667	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	146.55658	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	146.87649	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	147.1964	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	147.51631	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	147.83622	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	148.15613	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	148.47604	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	148.79595	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	149.11586	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	149.43577	Y	75.792766																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	45																																											
																																																	
G01					Z	45																																											
G01	X	10	Y	10																																													
;M84 ; turn off motors																																																	
