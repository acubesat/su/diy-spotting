G21																																																	
G90																																																	
G17																																																	
G01 					Z	45																																											
;spots 1st line																																																	
        ;water-cells1																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;1 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.823	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.246	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.669	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	136.092	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	136.515	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.938	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	137.361	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	137.784	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	138.207	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	138.63	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	139.053	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	139.476	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	139.899	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	140.322	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	140.745	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	141.168	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	141.591	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	142.014	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	142.437	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	142.86	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	143.283	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	143.706	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	144.129	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	144.552	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	144.975	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	145.398	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	145.821	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	146.244	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	146.667	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	147.09	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	147.513	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	147.936	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	148.359	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	148.782	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	149.205	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	149.628	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	150.051	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	150.474	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	150.897	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	151.32	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	151.743	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	152.166	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	152.589	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	153.012	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	153.435	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	153.858	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	154.281	Y	69.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	40																																											
																																																	
;spots 2nd line																																																	
        ;water-cells2																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;2 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.823	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.246	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.669	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	136.092	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	136.515	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.938	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	137.361	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	137.784	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	138.207	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	138.63	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	139.053	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	139.476	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	139.899	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	140.322	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	140.745	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	141.168	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	141.591	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	142.014	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	142.437	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	142.86	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	143.283	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	143.706	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	144.129	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	144.552	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	144.975	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	145.398	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	145.821	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	146.244	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	146.667	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	147.09	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	147.513	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	147.936	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	148.359	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	148.782	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	149.205	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	149.628	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	150.051	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	150.474	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	150.897	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	151.32	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	151.743	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	152.166	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	152.589	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	153.012	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	153.435	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	153.858	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	154.281	Y	70.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	40																																											
																																																	
;spots 3rd line																																																	
        ;water-cells3																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;3 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.823	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.246	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.669	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	136.092	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	136.515	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.938	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	137.361	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	137.784	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	138.207	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	138.63	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	139.053	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	139.476	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	139.899	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	140.322	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	140.745	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	141.168	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	141.591	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	142.014	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	142.437	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	142.86	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	143.283	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	143.706	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	144.129	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	144.552	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	144.975	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	145.398	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	145.821	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	146.244	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	146.667	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	147.09	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	147.513	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	147.936	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	148.359	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	148.782	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	149.205	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	149.628	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	150.051	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	150.474	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	150.897	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	151.32	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	151.743	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	152.166	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	152.589	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	153.012	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	153.435	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	153.858	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	154.281	Y	71.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	40																																											
																																																	
;spots 4th line																																																	
        ;water-cells4																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;4 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.823	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.246	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.669	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	136.092	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	136.515	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.938	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	137.361	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	137.784	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	138.207	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	138.63	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	139.053	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	139.476	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	139.899	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	140.322	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	140.745	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	141.168	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	141.591	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	142.014	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	142.437	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	142.86	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	143.283	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	143.706	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	144.129	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	144.552	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	144.975	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	145.398	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	145.821	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	146.244	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	146.667	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	147.09	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	147.513	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	147.936	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	148.359	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	148.782	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	149.205	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	149.628	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	150.051	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	150.474	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	150.897	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	151.32	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	151.743	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	152.166	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	152.589	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	153.012	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	153.435	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	153.858	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	154.281	Y	71.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	40																																											
																																																	
;spots 5th line																																																	
        ;water-cells5																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;5 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.823	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.246	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.669	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	136.092	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	136.515	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.938	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	137.361	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	137.784	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	138.207	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	138.63	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	139.053	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	139.476	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	139.899	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	140.322	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	140.745	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	141.168	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	141.591	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	142.014	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	142.437	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	142.86	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	143.283	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	143.706	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	144.129	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	144.552	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	144.975	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	145.398	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	145.821	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	146.244	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	146.667	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	147.09	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	147.513	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	147.936	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	148.359	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	148.782	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	149.205	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	149.628	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	150.051	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	150.474	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	150.897	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	151.32	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	151.743	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	152.166	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	152.589	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	153.012	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	153.435	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	153.858	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	154.281	Y	72.58																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	40																																											
																																																	
;spots 6th line																																																	
        ;water-cells6																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;6 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.823	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.246	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.669	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	136.092	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	136.515	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.938	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	137.361	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	137.784	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	138.207	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	138.63	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	139.053	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	139.476	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	139.899	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	140.322	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	140.745	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	141.168	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	141.591	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	142.014	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	142.437	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	142.86	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	143.283	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	143.706	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	144.129	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	144.552	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	144.975	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	145.398	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	145.821	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	146.244	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	146.667	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	147.09	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	147.513	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	147.936	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	148.359	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	148.782	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	149.205	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	149.628	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	150.051	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	150.474	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	150.897	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	151.32	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	151.743	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	152.166	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	152.589	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	153.012	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	153.435	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	153.858	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	154.281	Y	73.33																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	40																																											
																																																	
;spots 7th line																																																	
        ;water-cells7																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;7 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.823	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.246	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.669	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	136.092	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	136.515	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.938	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	137.361	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	137.784	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	138.207	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	138.63	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	139.053	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	139.476	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	139.899	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	140.322	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	140.745	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	141.168	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	141.591	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	142.014	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	142.437	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	142.86	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	143.283	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	143.706	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	144.129	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	144.552	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	144.975	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	145.398	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	145.821	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	146.244	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	146.667	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	147.09	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	147.513	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	147.936	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	148.359	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	148.782	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	149.205	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	149.628	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	150.051	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	150.474	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	150.897	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	151.32	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	151.743	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	152.166	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	152.589	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	153.012	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	153.435	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	153.858	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	154.281	Y	74.08																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	40																																											
																																																	
;spots 8th line																																																	
        ;water-cells8																																																	
            ;wash in water																																																	
                    G01					Z	45																																											
                    G01 	X	142.5	Y	132																																													
                    G01 					Z	30																																											
                    G04									S	2																																							
                    G01 					Z	45																																											
            ;fan clean 																																																	
                    G01					Z	45																																											
                    G01	X	40	Y	142																																													
                    G01					Z	32																																											
                    M106																																																	
                    G04									S	4																																							
                    M107																																																	
                    G01 					Z	45																																											
            ;spore uptake																																																	
                   G01					Z	45																																											
                   G01	X	133.5	Y	132																																													
                   G01					Z	30																																											
                   G04									S	2																																							
                   G01					Z	45																																											
        ;8 spotting																																																	
                ;1st chamber																																																	
                    G01					Z	45																																											
                    G01	X	134.4	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	45																																											
                ;2nd chamber																																																	
                    G01	X	134.823	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;3rd chamber																																																	
                    G01	X	135.246	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;4th chamber																																																	
                    G01	X	135.669	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;5th chamber																																																	
                    G01	X	136.092	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;6th chamber																																																	
                    G01	X	136.515	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;7th chamber																																																	
                    G01	X	136.938	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;8th chamber																																																	
                    G01	X	137.361	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;9th chamber																																																	
                    G01	X	137.784	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;10th chamber																																																	
                    G01	X	138.207	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;11th chamber																																																	
                    G01	X	138.63	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;12th chamber																																																	
                    G01	X	139.053	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;13th chamber																																																	
                    G01	X	139.476	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;14th chamber																																																	
                    G01	X	139.899	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;15th chamber																																																	
                    G01	X	140.322	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;16th chamber																																																	
                    G01	X	140.745	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;17th chamber																																																	
                    G01	X	141.168	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;18th chamber																																																	
                    G01	X	141.591	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;19th chamber																																																	
                    G01	X	142.014	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;20th chamber																																																	
                    G01	X	142.437	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;21st chamber																																																	
                    G01	X	142.86	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;22nd chamber																																																	
                    G01	X	143.283	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;23rd chamber																																																	
                    G01	X	143.706	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;24th chamber																																																	
                    G01	X	144.129	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;25th chamber																																																	
                    G01	X	144.552	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;26th chamber																																																	
                    G01	X	144.975	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;27th chamber																																																	
                    G01	X	145.398	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;28th chamber																																																	
                    G01	X	145.821	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;29th chamber																																																	
                    G01	X	146.244	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;30th chamber																																																	
                    G01	X	146.667	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;31st chamber																																																	
                    G01	X	147.09	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;32nd chamber																																																	
                    G01	X	147.513	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;33rd chamber																																																	
                    G01	X	147.936	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;34th chamber																																																	
                    G01	X	148.359	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;35th chamber																																																	
                    G01	X	148.782	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;36th chamber																																																	
                    G01	X	149.205	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;37th chamber																																																	
                    G01	X	149.628	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;38th chamber																																																	
                    G01	X	150.051	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;39th chamber																																																	
                    G01	X	150.474	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;40th chamber																																																	
                    G01	X	150.897	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;41st chamber																																																	
                    G01	X	151.32	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;42nd chamber																																																	
                    G01	X	151.743	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;43rd chamber																																																	
                    G01	X	152.166	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;44th chamber																																																	
                    G01	X	152.589	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;45th chamber																																																	
                    G01	X	153.012	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;46th chamber																																																	
                    G01	X	153.435	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;47th chamber																																																	
                    G01	X	153.858	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                ;48th chamber																																																	
                    G01	X	154.281	Y	74.83																																													
                    G01					Z	31																																											
                    G04									S	2																																							
                    G01					Z	39																																											
                    G01					Z	40																																											
																																																	
G01					Z	45																																											
G01	X	10	Y	10																																													
;M84 ; turn off motors																																																	
