G21																							
G90																							
G17																							
G01 					Z	45																	
																							
;3rd collumn spotting																							
    ;F3 process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;F3 spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	151.5	Y	150																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;E3process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;E3-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	151.5	Y	159																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;D3process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;D3-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	151.5	Y	168																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;C3process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;C3-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	151.5	Y	177																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;B3process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;B3-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	151.5	Y	186																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;A3process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;A3-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	151.5	Y	195																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
																							
;spots 4th line																							
    ;F3process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;F4-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	160.5	Y	150																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;E4process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;E4-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	160.5	Y	159																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;D4process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;D4-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	160.5	Y	168																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;C4process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;C4-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	160.5	Y	177																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;B4process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;B4-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	160.5	Y	186																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;A4process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;A4-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	160.5	Y	195																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
;spots 5th line																							
    ;F5process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;F5-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	169.5	Y	150																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;E5process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	169.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;E5-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	169.5	Y	159																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;D5process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	169.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;D5-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	169.5	Y	168																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;C5process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;C5-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	169.5	Y	177																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;B5process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;B5-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	169.5	Y	186																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;A5process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;A5-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	169.5	Y	195																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
																							
;spots 6th line																							
    ;F6process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;F6-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	178.5	Y	150																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;E6process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;E6-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	178.5	Y	159																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;D6process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;D6-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	178.5	Y	168																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;C6process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;C6-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	178.5	Y	177																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;B6process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;B6-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	178.5	Y	186																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;A6process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;A6-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	178.5	Y	195																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
																							
;7th collumn spotting																							
    ;F7 process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;F7 spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	187.5	Y	150																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;E7process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;E7-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	187.5	Y	159																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;D7process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;D7-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	187.5	Y	168																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;C7process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;C7-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	187.5	Y	177																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;B7process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;B7-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	187.5	Y	186																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;A7process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;A7-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	187.5	Y	195																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
																							
;spots 8th line																							
    ;F8process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;F8-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	196.5	Y	150																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;E8process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;E8-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	196.5	Y	159																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;D8process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;D8-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	196.5	Y	168																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;C8process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;C8-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	196.5	Y	177																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;B8process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;B8-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	196.5	Y	186																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;A8process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;A8-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	196.5	Y	195																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
;spots 9th line																							
    ;F9process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;F9-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	205.5	Y	150																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;E9process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	205.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;E9-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	205.5	Y	159																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;D9process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	205.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;D9-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	205.5	Y	168																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;C9process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;C9-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	205.5	Y	177																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;B9process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;B9-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	205.5	Y	186																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;A9process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;A9-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	205.5	Y	195																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
																							
;spots 10th line																							
    ;F10process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;F10-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	214.5	Y	150																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;E10process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;E10-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	214.5	Y	159																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;D10process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;D10-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	214.5	Y	168																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;C10process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;C10-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	214.5	Y	177																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;B10process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;B10-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	214.5	Y	186																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;A10process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;A10-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	214.5	Y	195																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
;spots 11th line																							
    ;F11process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;F11-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	223.5	Y	150																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;E11process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	223.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;E11-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	223.5	Y	159																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;D11process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	223.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;D11-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	223.5	Y	168																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;C11process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;C11-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	223.5	Y	177																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;B11process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;B11-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	223.5	Y	186																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;A11process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;A11-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	223.5	Y	195																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
																							
;spots 12th line																							
    ;F12process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;F12-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	232.5	Y	150																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;E12process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;E12-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	232.5	Y	159																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;D12process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;D12-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	232.5	Y	168																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;C12process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;C12-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	232.5	Y	177																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
    ;B12process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;B12-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	232.5	Y	186																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
    ;A12process																							
        ;water-cells																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
            ;spore uptake																							
                   G01					Z	45																	
                   G01	X	133.5	Y	132																			
                   G01					Z	30																	
                   G04									S	2													
                   G01					Z	45																	
																							
        ;A12-spotting (x5)																							
                    G01					Z	45																	
                    G01 	X	232.5	Y	195																			
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	27																	
                    G04									S	2													
                    G01 					Z	31																	
																							
                    G01 					Z	45																	
																							
																							
																							
																							
;final-wash																							
            ;wash in water																							
                    G01					Z	45																	
                    G01 	X	142.5	Y	132																			
                    G01 					Z	30																	
                    G04									S	2													
                    G01 					Z	45																	
            ;fan clean 																							
                    G01					Z	45																	
                    G01	X	40	Y	142																			
                    G01					Z	32																	
                    M106																							
                    G04									S	4													
                    M107																							
                    G01 					Z	45																	
																							
G01					Z	50																	
G01	X	15	Y	210																			
;M84 ; turn off motors																							
